var createError = require('http-errors')
var express = require('express')
var path = require('path')
var cookieParser = require('cookie-parser')
var logger = require('morgan')
var ent = require('ent')
var app = express()

//const http = require('http')
//const https = require('https')
const fs = require('fs')

const options = {
  key: fs.readFileSync('ssl/selfsigned.key'),
  cert: fs.readFileSync('ssl/selfsigned.crt')
}

//http.createServer(app).listen(3012)
//https.createServer(options, app).listen(3012)

//var io = require('socket.io').listen(http);

var indexRouter = require('./routes/index')
var usersRouter = require('./routes/users')
var rssRouter = require('./routes/rss')

/*
io.sockets.on('connection', function (socket) {
  socket.on('get_rss', function(message) {
    console.log('RSS:' + 'Connected')
    message = ent.encode(message);
  });
  socket.on('add_rss_url', function (message) {
    message = ent.encode(message);
  });
  socket.on('remove_rss_url', function (message) {
    message = ent.encode(message);
  });
  socket.on('ping', function (message) {
    message = ent.encode(message);
    console.log('ping' + message);
  });
});
*/
// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'jade')

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use('/', indexRouter)
app.use('/users', usersRouter)
app.use('/rss', rssRouter)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404))
})

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

module.exports = app
