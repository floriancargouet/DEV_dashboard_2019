const express = require('express');
const router = express.Router();
const async = require('express-async-await')
const fetch = require('node-fetch');
// global.Headers = fetch.Headers;
const clientId = "bb0c749c6403fd2";
const base = "gallery/";
const section = "search/";
const sort = "viral/";
const page = "0.json";
const query = "?q=cat";

// var myHeaders = new Headers();
// myHeaders.append("Authorization", "Client-ID " + clientId);
// var formdata = new FormData();

// var requestOptions = {
//   method: 'GET',
//   headers: myHeaders,
//   body: formdata,
//   redirect: 'follow'
// };

var url = "https://api.imgur.com/3/" + base + section + sort + page  + query;

/* GET home page. */
router.get('/data', async function (req, res, next) {
  const processData = async () => {
    const imgur = await fetch(url, {headers: {Authorization: `Client-ID ${clientId}`}})
    // .then(res => res.json())
    // .then(json => console.log(json))
    const response = await imgur.json()
    return response;
  }
  const data = await processData();
  res.send(data)
  res.end()
});

module.exports = router;





