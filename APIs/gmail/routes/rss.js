var express = require('express')
var router = express.Router()

let Parser = require('rss-parser')
let parser = new Parser()

/* GET users listing. */
router.get('/:id', function (req, res, next) {
  console.log("params is set to " + req.params.id);
  var urls = [
    'https://www.reddit.com/.rss',
    'https://freefilesync.org/news-feed.php'
  ]

  Promise.all(urls.map(url => parser.parseURL(url))).then(results => {
    results.forEach((result, num) => {
      //console.log(result)
    })
    res.send(results)
  })
})

router.get('/', function (req, res, next) {
  //GET /test?color1=green&color2=blue
  //req.query.color1 === 'green'  // true
  //req.query.color2 === 'blue' // true

  var urls = [
    'https://www.reddit.com/.rss',
    'https://freefilesync.org/news-feed.php'
  ]

  Promise.all(urls.map(url => parser.parseURL(url))).then(results => {
    results.forEach((result, num) => {
      //console.log(result)
    })
    res.send(results)
  })
})

module.exports = router
