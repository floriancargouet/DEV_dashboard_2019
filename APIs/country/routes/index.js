const express = require('express');
const router = express.Router();
var http = require('http');
const async = require('express-async-await')
const fetch = require('node-fetch')
const APIKEY = "eb853b9456aeab69078abce6b66c7c65";
let baseURL = "http://api.openweathermap.org/data/2.5/weather?q=";
let url = "".concat(baseURL, "&appid=", APIKEY, "&units=metric");

/* GET home page. */
router.get('/', async function (req, res, next) {
  const processData = async () => {
    const weather = await fetch(url) 
    const response = await weather.json()
    return response;
  }
  const data = await processData();
  res.send(data)
  res.end()
});

module.exports = router;
