import React, { Component } from "react";
import { Link } from "react-router-dom";

import PropTypes from "prop-types";
import { connect } from "react-redux";
import { loginUser } from "../../actions/authActions";
import Logout from '../auth/Logout'
// import Login from '../auth/Login'

function LogButtons(props) {
	const isLoggedIn = props.isLoggedIn;
	if (isLoggedIn) {
		return <Logout />;
	}
	return "";
}

class Navbar extends Component {
	render() {
		return (
			<nav>
				<div className="nav-wrapper">
					{/* <a href="#" data-target="mobile-demo" className="sidenav-trigger"><i className="material-icons">menu</i></a> */}
					<ul className="">
						<li>
							<Link to="/" className="link">
								~/home
            				</Link>
						</li>
						<li className="btn waves-effect waves-light">
							<LogButtons isLoggedIn={this.props.auth.isAuthenticated} />
						</li>
					</ul>
				</div>
			</nav>
		);
	}
}


Navbar.propTypes = {
	loginUser: PropTypes.func.isRequired,
	auth: PropTypes.object.isRequired,
	errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
	auth: state.auth,
	errors: state.errors
});

export default connect(
	mapStateToProps,
	{ loginUser }
)(Navbar);

