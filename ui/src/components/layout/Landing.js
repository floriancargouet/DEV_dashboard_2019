import React, { Component } from "react";
import { Link } from "react-router-dom";
import Facebook from "../auth/facebook/Facebook"
import Google from "../auth/google/Google"
import Microsoft from "../auth/microsoft/Microsoft"
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { loginUser } from "../../actions/authActions";

class Landing extends Component {
  
  componentDidMount() {
    // If logged in and user navigates to Login page, should redirect them to dashboard
    if (this.props.auth.isAuthenticated) {
      this.props.history.push("/dashboard");
    }
  }

  render() {
    return (
      <div style={{ height: "75vh" }} className="container valign-wrapper">
        <div className="row">
          <div className="col s12 center-align">
            <h4>
              <b>Dashboard</b> Epitech{" "}
              <span style={{ fontFamily: "monospace" }}>Pre-MSC</span> Nantes
            </h4>
            <p className="flow-text grey-text text-darken-1">
              Check our projetct on GitLab: <a href="https://gitlab.com/azertyuio">https://gitlab.com/azertyuio</a>
            </p>
            <br />
            <div className="col s6">
              <Link
                to="/register"
                style={{
                  width: "140px",
                  borderRadius: "3px",
                  letterSpacing: "1.5px"
                }}
                className="btn btn-large waves-effect waves-light hoverable blue accent-3"
              >
                Register
              </Link>
            </div>
            <div className="col s6">
              <Link
                to="/login"
                style={{
                  width: "140px",
                  borderRadius: "3px",
                  letterSpacing: "1.5px"
                }}
                className="btn btn-large btn-flat waves-effect white black-text"
              >
                Log In
              </Link>
            </div>
            <Facebook/>
            <Google/>
            <Microsoft/>
          </div>
        </div>
      </div>
    );
  }
}

Landing.propTypes = {
  loginUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { loginUser }
)(Landing);
