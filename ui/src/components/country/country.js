import React, { Component } from 'react';
import openSocket from 'socket.io-client';

class Country extends Component {
    constructor(props) {
        super(props);
        this.searchCountry = this.searchCountry.bind(this);
      }
      inputId = 'input_' + Math.floor(Math.random() * 10000)
      countryId = 'country_' + Math.floor(Math.random() * 10000)
      regionId = 'region_' + Math.floor(Math.random() * 10000)
      capitalId = 'capital_' + Math.floor(Math.random() * 10000)
      peopleId = 'people_' + Math.floor(Math.random() * 10000)
      currencyId = 'currency_' + Math.floor(Math.random() * 10000)
      langageId = 'langage_' + Math.floor(Math.random() * 10000)

    searchCountry = function (inputVal) {
        const socket = openSocket('http://localhost:5559');
        console.log(inputVal);
        socket.emit('search', inputVal);
        var country;
        socket.on('from-api', data => {
            country = data;
            console.log(country);
            if (country[0].name === undefined)
                document.getElementById(this.countryId).innerHTML = 'Unknown country';
            else {
                document.getElementById(this.countryId).innerHTML = "Country : " + country[0].name;
                document.getElementById(this.regionId).innerHTML = "Region : " + country[0].region;
                document.getElementById(this.capitalId).innerHTML = "Capital city : " + country[0].capital;
                document.getElementById(this.peopleId).innerHTML = "People : " + country[0].demonym;
                document.getElementById(this.currencyId).innerHTML = "Currency : " + country[0].currencies[0].name + " " + country[0].currencies[0].symbol;
                document.getElementById(this.langageId).innerHTML = "Language : " + country[0].languages[0].name + " " + country[0].languages[0].iso639_1;
            }
            socket.disconnect();
        })
    };

    render() {
        return (
        <div>
            <div className="card teal">
                <div className="card-content white-text">
                    <input placeholder="Search a country" id={this.inputId}/>
                    <p id={this.countryId}>Country : --</p>
                    <p id={this.regionId}>Region : --</p>
                    <p id={this.capitalId}>Capital city : --</p>
                    <p id={this.peopleId}>People : --</p>
                    <p id={this.currencyId}>Currency : --</p>
                    <p id={this.langageId}>Language : --</p>        
                </div>
                <div className="card-content white">
                    <p className="btn-floating btn-large waves-effect waves-light teal" onClick={() => {
                var inputVal = document.getElementById(this.inputId).value;
                if (inputVal !== "")
                    this.searchCountry(inputVal)}}>
                <i className="material-icons">location_on</i></p>
                </div>
                </div>
        </div>
        );
    }
}

export default Country;