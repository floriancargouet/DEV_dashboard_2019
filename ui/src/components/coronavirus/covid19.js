import React, { Component } from "react";
import io from 'socket.io-client';
import M from "materialize-css";
import ReactVis from './ReactVis';

class Covid19 extends Component {
	constructor(props) {
		super(props)
		this.state = {
		}
	}
	dataHandle(country, slug) {
		console.log("ok");

		let socket = io('http://localhost:5558');
		country = country.textContent
		socket.emit('country', slug);
		socket.on('country-data', (data) => {
			data = JSON.parse(data)
			for (var i = 0; i < data.length - 1; i++) {
				console.log(i);
			}
			const reactVis = (
				<ReactVis key={i} data={data} />
			)
			
			if (i === 0) {
				this.setState({ ReactVis: "No data available" });
			} else {
				this.setState({ ReactVis: reactVis });
			}
			socket.disconnect()
		})
	}

	componentDidMount() {
		let socket = io('http://localhost:5558');
		var options = [];
		socket.on('from-api', (data) => {
			data = JSON.parse(data)
			for (var i = data.length - 1; i >= 0; i--) {
				var country = data[i].Country
				var slug = data[i].Slug
				// console.log(slug);
				const option = (
					<button className="btn" value={slug} key={i} onClick={(e) => this.dataHandle(e.currentTarget, e.currentTarget.value)} >{country}</button>
				);
				options.push(option);
			}
			this.setState({ Options: options });
			socket.disconnect()
			// var julie = document.querySelectorAll;
			// julie.addEventListener("mouseover", function (event) {
			// 	div.classList.add("anotherclass");

			// 	julie.scrollIntoView({
			// 		inline: "center"
			// 	});
			// })

		})
		M.AutoInit();
	}

	render() {
		return (
			<div className={this.props.className}>
				<h3>Confirmed cases COVID19</h3>
				<div className="input-field col s12 countries">
					{this.state.Options}
				</div>
				{console.log(this.state.ReactVis)}
				<div>
					{this.state.ReactVis}
				</div>
			</div>
		);
	}
}

export default Covid19;