import React, { Component } from 'react';
import '../../../node_modules/react-vis/dist/style.css';
import { XYPlot, VerticalBarSeries } from 'react-vis';

class ReactVis extends Component {
    componentDidMount() {
        console.log(this.props.data);
    }
    componentDidUpdate() {
        console.log(this.props.data);
    }
    render() {
        var confirmed = null;
        const data = [];
        for (let i = 0; i < this.props.data.length; i++) {
            confirmed = this.props.data[i].Confirmed
            data.push( { x: i, y: confirmed },)
        }
        console.log(data);


        return (
            <div className="ReactVis">
                <XYPlot height={300} width={300} style={{ marginTop: "100px" }}>
                    <VerticalBarSeries data={data} />
                </XYPlot>
            </div>
        );
    }
}

export default ReactVis;