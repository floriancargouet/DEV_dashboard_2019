import React, { Component } from "react";
import ImgCard from "./ImgCard.js";
import io from 'socket.io-client';


class SearchBarImg extends Component {
	constructor(props) {
		super(props)
		this.state = {}
	}
	searchChangeHandler(event) {
		const socket = io('http://localhost:5557');
		const searchTerm = event.target.value;
		if (searchTerm.length > 0) {

			socket.emit('search', searchTerm);
			socket.on('from-api', (data) => {
				data = JSON.parse(data)
				var results = data.data;
				console.log(results);
				var imgCards = [];
				for (var i = results.length - 1; i >= 0; i--) {
					const imgCard = (
						<ImgCard
							key={results[i].id}
							img={results[i]}
						/>
					);
					imgCards.push(imgCard);
				}
				this.setState({ rows: imgCards });
				socket.disconnect()
			})
		} else {
			this.setState({ rows: "null" });
		}
	}

	render() {
		return (
			<div className={this.props.className}>
				<h3>Search image</h3>
				<input
					onChange={this.searchChangeHandler.bind(this)}
					placeholder="Enter search term"
				/>
				<div className="ImgCards">{this.state.rows}</div>
			</div>
		);
	}
}

export default SearchBarImg;