import React from "react";

class ImgCard extends React.Component {

	render() {
		return (
			<div className="col" key={this.props.img.id}>
				<div className="card horizontal imgCard">
					<div className="card-image">
						<img alt="" src={"https://i.imgur.com/" + this.props.img.cover + ".jpg"} />
					</div>
					<div className="card-stacked">
						<div className="card-content">
							<p>{this.props.img.title}</p>
						</div>
						<div className="card-action">
							<a href={this.props.img.link} target="_blank" rel="noopener noreferrer">view</a>
							<div className="views">
								<i className="material-icons">remove_red_eye</i>
								<p>{this.props.img.views}</p>
							</div>
						</div>

					</div>
				</div>
			</div>
		);
	}
}

export default ImgCard;
