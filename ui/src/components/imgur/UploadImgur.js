import React, { Component } from 'react';
import openSocket from 'socket.io-client';

var file;
class UploadImgur extends Component {
  constructor(props) {
    super(props);
    this.uploadImage = this.uploadImage.bind(this);
  }

  selectImage() {
    const e = document.getElementsByClassName('i')[0].files[0]
    var FR= new FileReader();
    FR.addEventListener("load", function(e) {
      document.getElementById("img").src = e.target.result;
      file = e.target.result;
    });
    FR.readAsDataURL(e);

  }

  uploadImage() {
    if((document.getElementById("name").value !== "") && (document.getElementsByClassName('i')[0].files[0] !== undefined)) {
        const socket = openSocket('http://localhost:5557');
        socket.emit("Upload", {
          file : file,
          name : document.getElementById("name").value}
          );
          document.getElementById("name").value = "";
    }
  }


    render() {
      return (
        <div className="card">
          <div className="card-content">
            <h2>Imgur Upload</h2>
            <img id="img" height="150" width="200" alt=""/>
          </div>
          <div className="card-tabs">
          <input type="file" className="i" accept="image/*" onChange={this.selectImage.bind(this)}/>
          </div>
          <div className="card-content grey lighten-4">
            <input placeholder="Name your photo" id="name"/>
            <button className="waves-effect waves-light btn" id="btnUpload" onClick={this.uploadImage}><i className="material-icons right">get_app</i>Upload</button>
          </div>
        </div>
      )}
}

export default UploadImgur;