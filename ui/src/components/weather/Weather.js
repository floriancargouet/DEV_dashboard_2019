import React, { Component } from 'react';
import openSocket from 'socket.io-client';

class Weather extends Component {
    constructor(props) {
        super(props);
        this.searchCity = this.searchCity.bind(this);
      }
      inputId = 'input_' + Math.floor(Math.random() * 10000)
      cityId = 'city_' + Math.floor(Math.random() * 10000)
      tempId = 'temp_' + Math.floor(Math.random() * 10000)
      detailId = 'detail_' + Math.floor(Math.random() * 10000)
      iconId = 'icon_' + Math.floor(Math.random() * 10000)

    searchCity = function (inputVal) {
        const socket = openSocket('http://localhost:5556');
        console.log(inputVal);
        socket.emit('search', inputVal);
        var weather;
        socket.on('from-api', data => {
            weather = data;
            console.log(weather);
            if (weather.name === undefined){
                document.getElementById(this.cityId).innerHTML = 'Unknown city';
                document.getElementById(this.iconId).innerHTML = "sentiment_dissatisfied";
                document.getElementById(this.tempId).innerHTML = "";
                document.getElementById(this.detailId).innerHTML = "";
            }
            else{
            document.getElementById(this.cityId).innerHTML = weather.name;
            document.getElementById(this.tempId).innerHTML = weather.main.temp + "°C";
            document.getElementById(this.detailId).innerHTML = weather.weather[0].description;
            if(weather.weather[0].main === "Clear")
                document.getElementById(this.iconId).innerHTML = "brightness_5";
            else if (weather.weather[0].main === "Rain")
                document.getElementById(this.iconId).innerHTML = "invert_colors";
            else if (weather.weather[0].main === "Clouds")
                document.getElementById(this.iconId).innerHTML = "filter_drama"; 
            }
            socket.disconnect();
        })
    };

    render() {
        return (
        <div>
            <div className="card teal">
                <div className="card-content white-text">
                    <input placeholder="Search a city meteo" id={this.inputId}/>
                    <p id={this.cityId}>Search for a city</p>
                    <i id={this.iconId} className="material-icons"></i>               
                    <p id={this.tempId}></p>
                    <p id={this.detailId}></p>    
                </div>
                <div className="card-content white">
                    <p className="btn-floating btn-large waves-effect waves-light teal" onClick={() => {
                var inputVal = document.getElementById(this.inputId).value;
                if (inputVal !== "")
                    this.searchCity(inputVal)}}>
                <i className="material-icons">location_on</i></p>
                </div>
                </div>
        </div>
        );
    }
}

export default Weather;