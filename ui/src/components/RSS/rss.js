import React, { Component } from 'react'
import socketIOClient from 'socket.io-client'

class rss extends Component {
  rech_value = ''

  button_id = 'button_' + Math.floor(Math.random() * 10000)
  rech_id = 'search_' + Math.floor(Math.random() * 10000)
  list_id = 'list_' + Math.floor(Math.random() * 10000)
  list_header_id = 'list_header_' + Math.floor(Math.random() * 10000)

  reset (event) {
    var bar = document.getElementById(this.rech_id)
    bar.value = ''
    var div = document.getElementById(this.list_id)
    div.innerHTML = ''
    div.innerHTML +=
      "<a href='#!' class='collection-item active'> <h4 id=" +
      this.list_header_id +
      '>-</h4> </a>'
    div.innerHTML += "<a href='#!' class='collection-item'>-</a>"
  }

  rss_url (event) {
    const endpoint = 'http://localhost:3010'
    const socket = socketIOClient(endpoint)

    const url = document.getElementById(this.rech_id).value
    if (url === '') {
      return
    }

    socket.emit('get_rss', url)

    socket.on('callback_server', data => {
      console.log(data)
      if (data === 'Failed') {
        var header = document.getElementById(this.list_header_id)
        header.innerText = 'failed :/'
        return
      }

      var div = document.getElementById(this.list_id)
      div.innerHTML = ''
      div.innerHTML +=
        "<a href='" +
        data.link +
        "' class='collection-item active'> <h4 id=" +
        this.list_header_id +
        '>' +
        data.title +
        '</h4> </a>'

      data.items.forEach(item => {
        div.innerHTML +=
          "<a href='" +
          item.link +
          "' class='collection-item'> " +
          item.title +
          ' </a>'
      })

      socket.disconnect()
    })
  }

  render () {
    return (
      <div>
        <h4>RSS</h4>
        <input placeholder='Enter search term' id={this.rech_id} />
        <div class='section scrollspy'>
          <a
            href='#!'
            class='waves-effect waves-light btn'
            onClick={this.rss_url.bind(this)}
            id={this.button_id}
          >
            Accept
          </a>
          <a
            href='#!'
            class='waves-effect waves-light btn'
            onClick={this.reset.bind(this)}
          >
            Reset
          </a>
        </div>

        <div class='collection' id={this.list_id}>
          <a href='#!' class='collection-item active'>
            <h4 id={this.list_header_id}>-</h4>
          </a>
          <a href='#!' class='collection-item'>
            -
          </a>
          <a href='#!' class='collection-item'>
            -
          </a>
        </div>
      </div>
    )
  }
}

export default rss
