import React, { Component } from 'react'
import socketIOClient from 'socket.io-client'

class gmail extends Component {
  socket = ''
  rech_value = ''

  button_id = 'button_' + Math.floor(Math.random() * 100000)
  list_id = 'list_' + Math.floor(Math.random() * 100000)
  list_header_id = 'list_header_' + Math.floor(Math.random() * 100000)

  code_id = 'code_' + Math.floor(Math.random() * 100000)
  button_code_id = 'button_' + Math.floor(Math.random() * 100000)

  get_header_name (headers, index) {
    for (const element of headers) {
      if (index === element.name) {
        return element.value
      }
    }
    return ''
  }

  reset (event) {
    var rech = document.getElementById(this.code_id)
    rech.innerHTML = ''
    var div = document.getElementById(this.list_id)
    div.innerHTML = ''
    div.innerHTML += "<a href='#!' class='collection-item'>-</a>"
    div.innerHTML += "<a href='#!' class='collection-item'>-</a>"
    if (this.socket !== '') {
      this.socket.disconnect()
      this.socket = ''
    }
  }

  connect_to_account (event) {
    const code = document.getElementById(this.code_id).value
    if (code === '' || this.socket === '') {
      return
    }
    this.socket.emit('get_key', code)
    this.socket.on('reset', data => {
      var div = document.getElementById(this.list_id)
      div.innerHTML = ''
    })
    this.socket.on('get_mail', data => {
      var div = document.getElementById(this.list_id)
      div.innerHTML +=
        "<a href='#!' class='collection-item'> " +
        this.get_header_name(data.data.payload.headers, 'Subject') +
        ' </a>'
    })
  }

  connect_gmail (event) {
    this.socket = socketIOClient('http://localhost:3015')
    this.socket.emit('connect_gmail', '')

    this.socket.on('callback_server', data => {
      console.log(data)

      if (data === 'Failed') {
        var header = document.getElementById(this.list_header_id)
        header.innerText = 'failed :/'
        return
      }
      var win = window.open(data, '_blank')
      win.focus()
    })
  }

  render () {
    return (
      <div>
        <h4>Gmail</h4>
        <div class='section scrollspy'>
          <a
            href='#!'
            class='waves-effect waves-light btn'
            onClick={this.connect_gmail.bind(this)}
            id={this.button_id}
          >
            Connect
          </a>
          <a
            href='#!'
            class='waves-effect waves-light btn'
            onClick={this.reset.bind(this)}
          >
            Disconnect
          </a>
        </div>
        <div>
          <input placeholder='code google' id={this.code_id} />
          <a
            href='#!'
            class='waves-effect waves-light btn'
            onClick={this.connect_to_account.bind(this)}
            id={this.button_code_id}
          >
            Accept
          </a>
        </div>
        <div>Mails :</div>
        <div class='collection' id={this.list_id}>
          <a href='#!' class='collection-item'>
            -
          </a>
          <a href='#!' class='collection-item'>
            -
          </a>
        </div>
      </div>
    )
  }
}

export default gmail
