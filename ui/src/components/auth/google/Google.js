import React, { Component } from 'react'
import { GoogleLogin } from 'react-google-login'

class Google extends Component {
  responseGoogle = response => {
    console.log(response)
  }
  componentClicked = () => console.log('clicked')
  clientId =
    '472557007332-99pcm6c26v5jb1qevfvb0hfjs1bq21v2.apps.googleusercontent.com'

  render () {
    let googlebutton = (
      <GoogleLogin
        clientId={this.clientId}
        //onSuccess={success}
        //onFailure={error}
        //onRequest={loading}
        offline={false}
        approvalPrompt='force'
        responseType='id_token'
        //isSignedIn
        theme='dark'
        redirectUri="http://localhost/callbackgoogle"
        // disabled
        // prompt="consent"
        // className='button'
        // style={{ color: 'red' }}
      >
        <span>Auth</span>
      </GoogleLogin>
    )

    return <div> {googlebutton} </div>
  }
}

export default Google
