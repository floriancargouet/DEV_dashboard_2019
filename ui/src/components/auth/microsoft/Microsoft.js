import React, { Component } from 'react';
import MicrosoftLogin from "react-microsoft-login";


class Microsoft extends Component {
    render() {
        const authHandler = (err, data) => {
            console.log(err, data);
          };
        return (
            <MicrosoftLogin clientId="abf721fd-51d7-4feb-b414-6be2e5a907c2" authCallback={authHandler} />
        );
    }
}
export default Microsoft;