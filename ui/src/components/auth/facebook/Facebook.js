import React, { Component } from 'react'
import FacebookLogin from 'react-facebook-login'

class Facebook extends Component {
  state = {
    isloggedIn: false,
    userID: '',
    name: '',
    email: '',
    picture: ''
  }

  responseFacebook = response => {
    console.log(response)
    try {
        // eslint-disable-next-line
        var name = response["name"];
        // eslint-disable-next-line
        var email = response["email"];
        // eslint-disable-next-line
        var id = response["id"];
        // eslint-disable-next-line
        var accessToken = response["accessToken"];

    } catch (error) {
      console.error(error)
    }
  }

  componentClicked = () => console.log('clicked')

  render () {
    let fbContent

    if (this.state.isloggedIn) {
      fbContent = null
    } else {
      fbContent = (
        <FacebookLogin
          appId='2693376667568503'
          autoLoad={false}
          fields='name,email,picture'
          onClick={this.componentClicked}
          callback={this.responseFacebook}
        />
      )
    }
    return <div>{fbContent}</div>
  }
}

export default Facebook
