import React from "react";



class MovieCard extends React.Component {

  viewMovie() {
    const url = "https://www.themoviedb.org/movie/" + this.props.movie.id;
    console.log(url)
    window.location.href = url;
  }

  render() {
    return (
    <div className="col"  key={this.props.movie.id}>
        <div className="card horizontal">
          <div className="card-image">
          <img alt={"poster-" + this.props.movie.title} src={this.props.posterSrc} />
          </div>
          <div className="card-stacked">
            <div className="card-content">
              <p>{this.props.movie.title}</p>
            </div>
            <div className="card-action" >
              <a href={"https://www.themoviedb.org/movie/" + this.props.movie.id} target="_blank" rel="noopener noreferrer">View more</a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default MovieCard;
