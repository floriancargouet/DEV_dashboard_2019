import React, { Component } from "react";
import MovieCard from "./MovieCard.js";
import io from 'socket.io-client';

class SearchBarMovie extends Component {
	constructor(props) {
		super(props)
		this.state = {}
	}
	searchChangeHandler(event) {
		const socket = io('http://localhost:5555');
		const searchTerm = event.target.value;
		if (searchTerm.length > 0) {
			socket.emit('search', searchTerm);
			socket.on('from-api', (data) => {
				data = JSON.parse(data)
				var results = data.results;
				console.log(results);

				var movieCards = [];
				for (var i = results.length - 1; i >= 0; i--) {
					results.poster_src = "https://image.tmdb.org/t/p/w185" + results[i].poster_path;
					const movieCard = (
						<MovieCard
							key={results[i].id}
							movie={results[i]}
							posterSrc={results.poster_src}
						/>
					);
					movieCards.push(movieCard);
				}
				this.setState({ rows: movieCards });
				socket.disconnect()
			})
		} else {
			this.setState({ rows: "null" });
		}
	}

	render() {
		return (
			<div className={this.props.className}>
				<h3>Search movie</h3>
				<input
					onChange={this.searchChangeHandler.bind(this)}
					placeholder="Enter search term"
				/>
				<div className="movieCards">
					{this.state.rows}
				</div>
			</div>
		);
	}
}

export default SearchBarMovie;