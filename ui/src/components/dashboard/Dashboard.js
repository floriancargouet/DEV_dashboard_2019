import React, { Component } from 'react'
import SearchBarMovie from '../movieDB/SeachBarMovie'
import SearchBarImg from '../imgur/SearchBarImg'
import Weather from '../weather/Weather'
import Covid19 from '../coronavirus/covid19'
import RSS from '../RSS/rss'
import UploadImgur from '../imgur/UploadImgur'
import Gmail from '../gmail/gmail'
import Country from '../country/country'

class Dashboard extends Component {
  render () {
    return (
      <div>
        <div className='row widgets'>
          <SearchBarImg className='Search col s12 m4 l4' />
          <Covid19 className='col s12 m4 l4' />
          <UploadImgur className='col s12 m4 l4' />
        </div>
        <div className='row widgets'>
          <div className='col s12 m4 l4'>
            <RSS />
          </div>
          <div className='col s12 m4 l4'>
            <Country />
          </div>
          <SearchBarMovie className='Search col s12 m4 l4' />
        </div>
        <div className='row widgets'>
          <SearchBarMovie className='Search col s12 m4 l4' />
          <div className='col s12 m4 l4'>
            <Weather />
          </div>
          <div className='col s12 m4 l4'>
            <RSS />
          </div>
        </div>
        <div className='row widgets'>
          <div className='col s12 m4 l4'>
            <Gmail />
          </div>
          <div className='col s12 m4 l4'>
            <Gmail />
          </div>
          <div className='col s12 m4 l4'>
            <Gmail />
          </div>
        </div>
        <div className='row widgets'>
          <SearchBarImg className='Search col s12 m4 l4' />
          <Covid19 className='col s12 m4 l4' />
          <UploadImgur className='col s12 m4 l4' />
        </div>
      </div>
    )
  }
}

export default Dashboard
